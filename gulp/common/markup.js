const gulp = require('gulp')
const config = require('../config')
const production = config.production
const pug = require('gulp-pug')
const fs = require('fs')

gulp.task('markup', () => (
  gulp.src(config.directories.src.markup + '/*.pug')
    .pipe(pug({
      doctype: 'HTML',
      pretty: production,
      locals: {
        icon: name => fs.readFileSync(`./src/assets/icons/${name}.svg`),
        production
      }
    }))
    .on('error', config.onError)
    .pipe(gulp.dest(config.directories.public.markup))
))