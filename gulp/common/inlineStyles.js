const gulp = require('gulp')
const config = require('../config')
const inlineStyles = require('gulp-inline-css')

gulp.task('inlineStyles', () =>
  gulp
    .src(config.directories.public.markup + '/*.html')
    .pipe(
      inlineStyles({
        removeStyleTags: false,
        removeLinkTags: false
      })
    )
    .pipe(gulp.dest(config.directories.public.markup))
)
