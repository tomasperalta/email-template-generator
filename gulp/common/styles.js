const gulp = require('gulp')
const config = require('../config')
const sass = require('gulp-sass')

gulp.task('styles:main', () => 
  gulp.src(config.project.cssFiles)
    .pipe(sass())
    .on('error', config.onError)
    .pipe(gulp.dest(config.directories.public.styles))
)

gulp.task('styles:vendor', () => 
  gulp.src(config.project.cssVendor)
    .pipe(sass())
    .on('error', config.onError)
    .pipe(gulp.dest(config.directories.public.styles))
)

gulp.task('styles', gulp.series('styles:main', 'styles:vendor'))